"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    vecc_magn = np.sqrt(x*x + y*y + z*z)
    
    return  vecc_magn

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Interpolation der Daten für äquidistante Messwerte
    new_time = np.linspace(time.min(), time.max(), num=len(time), endpoint=True)
    new_data = np.interp(new_time, time, data)
    return (new_time, new_data)


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    # Berechne die Abtastfrequenz (Sampling Rate)
    sampling_rate = 1 / np.mean(np.diff(time))

    # Führe die FFT durch
    fft_result = np.fft.fft(x)
    
    # Berechne die Frequenzen
    frequencies = np.fft.fftfreq(len(x), d=1/sampling_rate)

    # Bestimme die positive Hälfte des Spektrums und die entsprechenden Frequenzen
    positive_half = fft_result[:len(fft_result)//2]
    positive_frequencies = frequencies[:len(frequencies)//2]

    # Berechne die Amplitude (Betrag) des Spektrums
    amplitude_spectrum = np.abs(positive_half)

    return amplitude_spectrum, positive_frequencies