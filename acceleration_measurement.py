import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_Ukulele.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sensor_data_dict = {sensor_settings_dict["ID"]:
                    {
                    "acceleration_x":[],
                    "acceleration_y":[],
                    "acceleration_z":[],
                    "timestamp": [] 
                    }
                    }

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
start_time = time.time()
passed_time = 0
while passed_time <= measure_duration_in_s:
    
    sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_x"].append(accelerometer.acceleration[0])
    sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_y"].append(accelerometer.acceleration[1])
    sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_z"].append(accelerometer.acceleration[2])
    sensor_data_dict[sensor_settings_dict["ID"]]["timestamp"].append(passed_time)
    
    
    time.sleep(0.001)
    end_time = time.time()
    passed_time = end_time - start_time
# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Speichern der Daten in der H5-Datei
with h5py.File(path_h5_file, 'w') as h5_file:
    group = h5_file.create_group(sensor_settings_dict["ID"])
    
    # Funktion zur Erstellung von Datasets mit Attributen
    def create_dataset_with_attribute(name, data, attribute_name, attribute_value):
        dataset = group.create_dataset(name, data=data)
        dataset.attrs[attribute_name] = attribute_value

    # Erstellung der Datasets mit Attributen
    create_dataset_with_attribute("acceleration_x", sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_x"], "unit", "m/s^2")
    create_dataset_with_attribute("acceleration_y", sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_y"], "unit", "m/s^2")
    create_dataset_with_attribute("acceleration_z", sensor_data_dict[sensor_settings_dict["ID"]]["acceleration_z"], "unit", "m/s^2")
    create_dataset_with_attribute("timestamp", sensor_data_dict[sensor_settings_dict["ID"]]["timestamp"], "unit", "s")

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
